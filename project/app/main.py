from fastapi import Depends, FastAPI, APIRouter
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy.ext.asyncio import AsyncSession
from pydantic import BaseModel, HttpUrl
from typing import Optional


from db import init_db, get_session
from routers import (
    token,
    users,
    products,
    orders,
)

app = FastAPI()

@app.on_event('startup')
async def init():
    await init_db()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(token.router, tags=["Authentication"])
app.include_router(users.router, tags=["Users"])
app.include_router(products.router, tags=["Product"])
app.include_router(orders.router, tags=["Orders"])
