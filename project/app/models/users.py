from enum import unique
from typing import Dict, Optional
from pydantic import EmailStr, constr
from sqlmodel import SQLModel, Field

Password = constr(regex=r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$")

class Users(SQLModel, table=True):
    email: EmailStr = Field(default=None, primary_key=True)
    user_id: Optional[int] = Field(default=None, primary_key=True)
    hashed_password: Optional[str]
    user_name: Optional[str]

class UserBase(SQLModel):
    email: Optional[EmailStr]
    user_id: Optional[int]
    user_name: Optional[str]

class UserInDB(SQLModel):
    email: Optional[EmailStr]
    user_id: Optional[int]
    user_name: Optional[str]

class NewUser(SQLModel):
    email: Optional[EmailStr]
    user_name: Optional[str]
    password: Password

class DBUser(UserBase):
    hashed_password: Optional[str]

class UpdateUser(SQLModel):
    user_name: Optional[str]
    password: Optional[Password]  # type: ignore
    password_old: Optional[str]  # type: ignore
