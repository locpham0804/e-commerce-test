from typing import Optional
from sqlmodel import SQLModel, Field, ARRAY, Column, String
from datetime import datetime
from typing import List


class Orders(SQLModel, table=True):
    order_id: Optional[int] = Field(default=None, primary_key=True)
    list_of_product: Optional[list[str]] = Field(default=None, sa_column=Column(ARRAY(String())))
    total_price: Optional[float]
    status: Optional[str]
    user_id: Optional[int] = Field(default=None, foreign_key="users.user_id")

class OrdersInDB(Orders):
    user_name: Optional[str]

class NewOrder(SQLModel):
    user_id: Optional[int]
    list_of_product: List[dict] = Field(default=None, sa_column=ARRAY(dict), nullable=True)
    