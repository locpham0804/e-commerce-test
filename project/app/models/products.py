from typing import Optional
from sqlmodel import SQLModel, Field
from datetime import datetime

from models.inventory import InventoryInDB

class Products(SQLModel, table=True):
    product_id: Optional[int] = Field(default=None, primary_key=True, foreign_key="inventory.product_id")
    product_name: Optional[str]
    price: Optional[float]
    category: Optional[str]
    description: Optional[str]
    primary_image: Optional[str]
    secondary_image: Optional[str]

class ProductInDB(InventoryInDB):
    product_id: Optional[int]
    product_name: Optional[str]
    price: Optional[float]
    category: Optional[str]
    description: Optional[str]
    primary_image: Optional[str]
    secondary_image: Optional[str]
    