from typing import Optional
from sqlmodel import SQLModel, Field
from datetime import datetime

class Inventory(SQLModel, table=True):
    product_id: Optional[int] = Field(default=None, primary_key=True)
    stock: Optional[int]
    status: Optional[str]

class InventoryInDB(SQLModel):
    stock: Optional[int]
    status: Optional[str]