from fastapi import APIRouter, Depends, Request, FastAPI, Security, HTTPException, status
from fastapi.responses import RedirectResponse
from sqlalchemy.future import select
from sqlalchemy.ext.asyncio import AsyncSession
from db import get_session
from .token import (
    db_exception,
    get_password_hash,
    get_current_active_user,
    get_current_active_admin,
)
from asyncpg import Record
import logging
from asyncpg.exceptions import PostgresError
from pydantic import EmailStr
from passlib.context import CryptContext
import aiohttp
from sqlalchemy import func
from datetime import datetime
import json
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker
import random

from models.orders import Orders, OrdersInDB, NewOrder
from models.users import Users, UserInDB
from models.products import Products
from models.inventory import Inventory


router = APIRouter()

@router.post("/orders")#, response_model=OrdersInDB)
async def create_order(
    body: NewOrder,
    session: AsyncSession = Depends(get_session),
):
    # create orders of user
    total_price = 0
    for i in body.list_of_product: 
        results = await session.execute(select(Products).where(Products.product_id==i['product_id']))
        results=results.first()
        price = results.Products.price
        total_price=total_price+price*i['quality']

        #update inventory products
        result = await session.execute(select(Inventory)
                            .where(Inventory.product_id == i['product_id']))
        result = result.scalars().one()
        new_stock=result.stock-i['quality']
        if new_stock==0:
            result.status='not available'
        result.stock=new_stock

        session.add(result)
        await session.commit()
    query_order = Orders(
                    order_id=None,
                    list_of_product=[str(i) for i in body.list_of_product], 
                    status='pending', 
                    user_id=body.user_id,
                    total_price=total_price
                    )
    session.add(query_order)
    await session.commit()
    return query_order

@router.get("/orders")#, response_model=list[OrdersInDB])
async def get_orders(
    session: AsyncSession = Depends(get_session),
    current_user: UserInDB = Depends(get_current_active_user)):
    results = await session.execute(select(Orders, Users).join(Users, isouter=True)
                                .where(Orders.user_id == current_user.get("user_id")))
    results=results.all()
    return [OrdersInDB(**r.Orders.dict(), user_name=r.Users.user_name) for r in results]