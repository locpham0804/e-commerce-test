from fastapi import APIRouter, Depends, HTTPException, status, Security
from pydantic import BaseModel, EmailStr, ValidationError
from passlib.context import CryptContext
from fastapi.security import (
    OAuth2PasswordBearer,
    SecurityScopes,
)
from jose import JWTError, jwt
from typing import Optional
from datetime import datetime, timedelta

from sqlmodel import select
from db import get_session
from models.users import Users
from sqlalchemy.ext.asyncio import AsyncSession
from fastapi.responses import RedirectResponse
from fastapi.param_functions import Form

SECRET_KEY = "a2a3aa001a66f77b84301c0dafe96c29915562874ef0b3cacfe526e1debd67ba"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 120

router = APIRouter()

class Token(BaseModel):
    access_token: str
    token_type: str
    time_out: str


class TokenData(BaseModel):
    email: EmailStr
    scopes: list[str] = []


class LoginForm:
    # email: EmailStr
    # password: str
    # scopes: Optional[str] = None
    def __init__(
        self,
        username: EmailStr = Form(...),
        password: str = Form(...),
        scope: str = Form(""),
    ):
        self.email = username
        self.password = password
        self.scopes = scope.split()


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl="token",
    scopes={
        "me": "Read information about the current user.",
        "admin": "Read company.",
        # "super": "Read users in company.",
    },
)

def verify_password(plain_password: str, hashed_password: str):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password: str):
    return pwd_context.hash(password)


def db_exception(error: str):
    return HTTPException(status_code=status.HTTP_409_CONFLICT, detail=error)


async def get_user(session, email: EmailStr):
    result = await session.execute(select(Users).where(Users.email==email))
    users = result.scalars().first()
    if not users:
        return users
    return users.dict()


async def authenticate_user(session, email: EmailStr, password: str):
    user = await get_user(session, email)
    if not user:
        return False
    if not verify_password(password, user.get("hashed_password")):
        return False
    return user


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(
    security_scopes: SecurityScopes,
    token: str = Depends(oauth2_scheme),
    session: AsyncSession = Depends(get_session),
):

    if security_scopes.scopes:
        authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'
    else:
        authenticate_value = f"Bearer"

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": authenticate_value},
    )

    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        email: EmailStr = payload.get("sub")
        if email is None:
            raise credentials_exception
        token_scopes = payload.get("scopes", [])
        token_data = TokenData(scopes=token_scopes, email=email)
    except (JWTError, ValidationError):
        raise credentials_exception
    user = await get_user(session, email=token_data.email)
    if user is None:
        raise credentials_exception
    for scope in security_scopes.scopes:
        if scope not in token_data.scopes:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Not enough permissions",
                headers={"WWW-Authenticate": authenticate_value},
            )
    return user


async def get_current_active_user(user=Security(get_current_user, scopes=["me"])):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Insufficient permission",
        headers={"WWW-Authenticate": "Bearer"},
    )
    if user:
        return user
    else:
        raise credentials_exception


async def get_current_active_admin(
    user=Security(get_current_user, scopes=["me", "admin"])
):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Insufficient permission",
        headers={"WWW-Authenticate": "Bearer"},
    )
    if not user.get("is_superuser") and not user.get("is_admin"):
        raise credentials_exception
    else:
        return user


@router.post("/token", response_model=Token)
async def login_for_access_token(
    form_data: LoginForm = Depends(), session: AsyncSession = Depends(get_session)
):
    if not form_data.email:
        form_data.email = (form_data.username).lower()
    
    form_data.email=(form_data.email).lower()
    user = await authenticate_user(session, form_data.email, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={
            "sub": user.get("email"),
            "is_admin": user.get("is_admin"),
            "is_superuser": user.get("is_superuser"),
            "scopes": form_data.scopes,
        },
        expires_delta=access_token_expires,
    )
    return {
        "access_token": access_token,
        "token_type": "bearer",
        "time_out": ACCESS_TOKEN_EXPIRE_MINUTES * 60,
    }
