from fastapi import APIRouter, Depends, Request, FastAPI, Security, HTTPException, status
from fastapi.responses import RedirectResponse
from sqlalchemy.future import select
from sqlalchemy.ext.asyncio import AsyncSession
from db import get_session
from .token import (
    db_exception,
    get_password_hash,
    get_current_active_user,
    get_current_active_admin,
)
from asyncpg import Record
import logging
from asyncpg.exceptions import PostgresError
from pydantic import EmailStr
from passlib.context import CryptContext
import aiohttp
from sqlalchemy import func
from datetime import datetime
import json
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker
import random

from models.products import ProductInDB, Products
from models.inventory import Inventory, InventoryInDB

router = APIRouter()


@router.get("/product-all/", response_model=list[ProductInDB])
async def query_product_all(
    limit: int = 10,
    skip: int = 0,
    session: AsyncSession = Depends(get_session),
):
    count= 100

    results = await session.execute(select(Products, Inventory).join(Inventory, isouter=True).offset(skip).limit(limit if limit else count))
    results=results.all()
    return [ProductInDB(**r.Products.dict(), stock=r.Inventory.stock, status=r.Inventory.status) for r in results]

@router.get("/product-search/", response_model=list[ProductInDB])
async def query_product_all(
    name_product: str,
    limit: int = 10,
    skip: int = 0,
    session: AsyncSession = Depends(get_session),
):
    count= 100
    regex_name_product = "'" + "%" + str(name_product.replace(' ', '%%')) + "%" + "'"
    results = await session.execute(f"""
                SELECT products.*, inventory.stock, inventory.status
                FROM products
                LEFT JOIN inventory on products.product_id=inventory.product_id
                WHERE products.product_name LIKE {regex_name_product}
                LIMIT {limit} OFFSET {skip};
            """)
    return [ProductInDB(**r) for r in results]
