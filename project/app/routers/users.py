from fastapi import APIRouter, Depends, Request, FastAPI, Security, HTTPException, status
from fastapi.responses import RedirectResponse
from sqlalchemy.future import select
from sqlalchemy.ext.asyncio import AsyncSession
from db import get_session
from .token import (
    db_exception,
    get_password_hash,
    get_current_active_user,
    get_current_active_admin,
)
from asyncpg import Record
import logging
from asyncpg.exceptions import PostgresError
from pydantic import EmailStr
from passlib.context import CryptContext
import aiohttp
from sqlalchemy import func
from datetime import datetime
import json
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker
import random

from models.users import Users, UserInDB, NewUser , DBUser, UpdateUser #, UpdateUser, UpdateUserMe, Users, NewUserAdmin

router = APIRouter()

@router.post("/users", response_model=UserInDB)
async def create_user(
    body: NewUser,
    session: AsyncSession = Depends(get_session),
):
    user = DBUser(**body.dict(), hashed_password=get_password_hash(body.password))
    query_user = Users(
                            user_id=None,
                            email=user.email, 
                            hashed_password=user.hashed_password, 
                            user_name=user.user_name,)
    session.add(query_user)
    await session.commit()

    result = (await session.execute(select(Users).where(Users.email==user.email))).scalars().first()
    return result

@router.get("/users/me", response_model=UserInDB)
async def get_me(
    session: AsyncSession = Depends(get_session),
    current_user: UserInDB = Depends(get_current_active_user)):
    result = await session.execute(select(Users).select_from(Users)
                                .where(Users.email == current_user.get("email")))
    result=result.one()
    return UserInDB(**result.Users.dict())

@router.put("/users/me", response_model=UserInDB)
async def update_me(
    body: UpdateUser,
    current_user: Record = Depends(get_current_active_user),
    session: AsyncSession = Depends(get_session),
):
    result = await session.execute(select(Users)
                                    .where(Users.email == current_user.get("email")))
    user = result.scalars().one()
    if body.user_name:
        user.user_name=body.user_name
    if body.password:
        pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
        if pwd_context.verify(body.password_old, current_user.get("hashed_password")):
            user.hashed_password = get_password_hash(body.password)
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    try:
        session.add(user)
        await session.commit()
        result = await session.execute(select(Users).select_from(Users)
                                .where(Users.email == current_user.get("email")))
        result=result.one()
        return UserInDB(**result.Users.dict())
    except:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
